def measure(num = 1)
  run_times = []
  num.times do
    start_time = Time.now
    yield
    finish_time = Time.now
    run_times << finish_time - start_time
  end

  run_times.inject(0, :+) / run_times.length.to_f
end
